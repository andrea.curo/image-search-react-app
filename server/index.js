const express = require("express");
const bodyParser = require("body-parser");
const pino = require("express-pino-logger")();
const cors = require("cors");
const request = require("request");

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.json());
app.use(pino);
app.use(cors());
app.options("*", cors());

const unsplashService = require("./service");

app.get("/api/images", async (req, res) => {
  if (req && req.query && req.query.searchText) {
    const resBody = await unsplashService().queryImages(req.query.searchText);
    const resStatus = resBody.error ? 500 : 200;
    res.setHeader("Content-Type", "application/json");
    res.status(resStatus).send(resBody);
  } else {
    res.status(400).send({ error: "Request body was malformed" });
  }
});

app.get("/api/images/:imageId/download", async (req, res) => {
  const url = await unsplashService().getImageUrlById(req.params.imageId);

  // TODO: give dynamic naming to files
  request(url)
    .on("response", (response) => {
      res.setHeader(
        "Content-Disposition",
        'attachment; filename="filename.jpg"'
      );
      res.setHeader("Content-Type", response.headers["content-type"]);
    })
    .pipe(res);
});

app.listen(3001, () =>
  console.log("Express server is running on localhost:3001")
);
