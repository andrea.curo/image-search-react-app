const Unsplash = require("unsplash-js").default;
const toJson = require("unsplash-js").toJson;
const sampleData = require("./sample-data.json");
const fetch = require("node-fetch");
global.fetch = fetch;

// Variable Setup
const APP_ACCESS_KEY = process.env.REACT_APP_APP_ACCESS_KEY || "";
const ENV = process.env.REACT_APP_ENV || "";
const devMode = ENV === "dev";

const unsplashService = () => {
  let unsplash;

  const setupApi = async () => {
    if (devMode && !unsplash) {
      console.log("dev mode set!");
      unsplash = {
        search: {
          photos: () => {
            return new Promise((res, rej) => {
              res(sampleData);
            });
          },
        },
        photos: {
          getPhoto: () => {
            return new Promise((res, rej) => {
              res(sampleData.results[0]);
            });
          },
        },
      };
    }

    if (!unsplash) {
      unsplash = new Unsplash({ accessKey: APP_ACCESS_KEY });
    }
  };

  const queryImages = async (text) => {
    setupApi();

    return unsplash.search
      .photos(text)
      .then(toJson)
      .then((data) => {
        const newData = [];
        data.results.map((result) => {
          const tmpUser = {
            username: result.user.username,
            url: result.user.links.html,
          };
          const tmpData = {
            id: result.id,
            description: result.description,
            links: result.links,
            urls: result.urls,
            user: tmpUser,
          };
          newData.push(tmpData);
        });
        return newData;
      })
      .catch((e) => {
        return { error: e };
      });
  };

  const getImageUrlById = async (id) => {
    setupApi();

    return unsplash.photos
      .getPhoto(id)
      .then(toJson)
      .then((json) => {
        return json.urls.regular;
      });
  };

  return {
    queryImages,
    getImageUrlById,
  };
};

module.exports = unsplashService;
