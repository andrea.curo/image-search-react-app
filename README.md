# Simple Image Search App

[[_TOC_]]

## Getting Started

### Requirements
Make sure you have git and node preinstalled to run this applicaiton. You will also need to obtain an API key from [unsplash-api](https://github.com/unsplash/unsplash-js). 

Create a `.env` file in the top-level directory with these properties set

```
REACT_APP_APP_ACCESS_KEY={your Unsplash API key}
REACT_APP_ENV={dev or prod}
REACT_APP_BASE_PATH=localhost:3001
REACT_APP_PROTOCOL=http
```
Setting `dev` will mock the Unsplash API

### Commands
> `git clone {ssh or https}`
> 
> `npm install`
> 
> `npm run-script dev`

`dev` script will run both applications at the same time

Only frontend application
> `npm run-script start`

Only API applicaiton
> `npm run-script server`

## Tools
- [ReactJS](https://reactjs.org/)
- [Node/Express](https://nodejs.org/en/)
- [Material-UI](https://material-ui.com/)
- [unsplash-api](https://github.com/unsplash/unsplash-js)
- [react-router](https://github.com/ReactTraining/react-router)
- [prettier](https://github.com/prettier/prettier)

## Rationale

### Why specific development technologies?

- My goal was to make a responsive and performant application. I chose React and Material with that in mind. In order to accomplish this quicky and efficiently, I chose other lightweight technologies like React-Router and kept the overall dependencies to a minimum.
- I prefer typescript for its static typing and structure, but, in order to quickly iterate on my solution, I decided to use JSX 

### Why specific architecture decisions?

- My first focus was performance and responsiveness, so I focused on the user experience and functionality of the app. Given more time, I would consider developing out other concerns like SEO, security, and accessibility. The basic structure for these concerns exist (eg. manifest.json, properties file).
- I selected Node and Express because they great technologies for communication with external api's. Frontend apps should also have a single point on interaction; this makes it way easier to track data sources. 
- I wanted to keep the code organized but easy to navigate, so I used a fairly basic folder structure to separate the main parts of the application.
- I used React Hooks; it's easier to read and uses less boilerplate.
- I used Data-Down approach. It's better to have a single point at which the data gets manipulated and to pass that point around. 
- I separated business logic into services that would interact with the API. /server/service.js handles all the communication with Unsplash and maps the data so that the front end can be maintained independently of the data source.

### Why specific implementation decisions?

- I added a delay to the search image functionality. It will wait until the user has stopped typing before performing a search. This is good practice for two reasons: it reduces repetitive calls to a third party api, and it reduces page renders to created a smoother user experience.
- When writing functions that interacted with an API, I created modules that would allow for private and public functions (ex. imageService.js). This let me control what gets exposed, whether it be constants or specific endpoints.
- I tested storing state data in a singular object as well as multiple variables. I found that one large object didn't save space and did muddle the clarity of the overall component. The component I tried it out on was ViewList.js.
- I used an auto code formatter for convenience. 
- I used partial components where able to help with readability.
- I saved string values to constants were applicable to help with maintainability in the future. In a client app, I would consider pulling as many constants out to a separate file so someone with minimum tech experience could easily change text on a page for example.

## TODO
- [ ] Add unit tests for all components
- [ ] Add consistent error responses to API
- [ ] Add API documentation
- [ ] Add multiple environment compilation features
- [ ] Check for Legacy Browser support
- [ ] Add form validation and error messaging
- [ ] Add pagination for image results
- [ ] Add dynamic file naming for image downloads
- [ ] Add user feedback for interations (eg. download, add to list)
- [ ] Do not allow users to add the same image to a list