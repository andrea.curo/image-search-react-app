import React, { useEffect, useState } from "react";

import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";

import Main from "./Pages/Main";
import Collections from "./Pages/Collections";
import ViewList from "./Pages/ViewList";

const useStyles = makeStyles({
  root: {
    minWidth: "100vw",
    minHeight: "100vh",
  },
  gutter: {
    margin: "20px",
  },
});

function App() {
  const classes = useStyles();
  const imageSearch = "image-search";
  const myLists = "my-lists";
  const [selectedTab, setSelectedTab] = useState(imageSearch);

  function handleChange(e, newVal) {
    setSelectedTab(newVal);
  }

  // Deps warning is ignored in order to run once on load
  useEffect(() => {
    setSelectedTab(
      window.location.pathname.indexOf("lists") !== -1 ? myLists : imageSearch
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className={classes.root}>
      <Router>
        <Paper>
          <Tabs
            value={selectedTab}
            onChange={handleChange}
            indicatorColor="primary"
            textColor="primary"
            centered>
            <Tab
              label="Image Search"
              value={imageSearch}
              to="/"
              component={Link}
            />
            <Tab
              label="My Lists"
              value={myLists}
              to="/lists"
              component={Link}
            />
          </Tabs>
        </Paper>

        <Switch>
          <Route exact path="/">
            <Main classes={classes} />
          </Route>
          <Route exact path="/lists">
            <Collections classes={classes} />
          </Route>
          <Route path="/lists/:listId">
            <ViewList />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
