import React, { useEffect, useState } from "react";

import TextField from "@material-ui/core/TextField";

import ImageCardWrapper from "../Components/ImageCard/ImageCardWrapper";
import ImageService from "../Components/ImageCard/imageService";

function Main(props) {
  const imageService = ImageService();
  const [searchText, setSearchText] = useState("");
  const [imageList, setImageList] = useState([]);
  const [searchTimeout, setSearchTimeout] = useState(null);

  function handleSearchChange(e) {
    setSearchText(e.target.value);
  }

  async function searchForImages() {
    const imageList = await imageService.queryImages(searchText);
    if (!imageList.err) {
      setImageList(imageList);
    }
  }

  useEffect(() => {
    if (searchText) {
      if (searchTimeout) {
        clearTimeout(searchTimeout);
      }

      setSearchTimeout(setTimeout(searchForImages, 300));
    } else if (searchText === "") {
      if (searchTimeout) {
        clearTimeout(searchTimeout);
      }
      setImageList([]);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [searchText]);

  return (
    <div>
      <form className={props.classes.gutter}>
        <TextField
          id="search-text"
          label="I want to see..."
          variant="outlined"
          value={searchText}
          fullWidth={true}
          onChange={handleSearchChange}
        />
      </form>
      <div className={props.classes.gutter}>
        <ImageCardWrapper imageList={imageList} />
      </div>
    </div>
  );
}

export default Main;
