import React, { useEffect, useState } from "react";
import ListCardWrapper from "../Components/ListCard/ListCardWrapper";
import ListService from "../Components/ListCard/ListService";

function Collections(props) {
  const listService = ListService();
  const [lists, setLists] = useState([]);

  // Deps warning is ignored in order to run once on load
  useEffect(() => {
    setLists(listService.getAllLists());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div>
      <div className={props.classes.gutter}>
        <ListCardWrapper lists={lists} />
      </div>
    </div>
  );
}

export default Collections;
