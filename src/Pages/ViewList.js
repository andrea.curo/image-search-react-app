import React, { useEffect, useState } from "react";

import { useParams } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import EditIcon from "@material-ui/icons/Edit";
import IconButton from "@material-ui/core/IconButton";
import SaveIcon from "@material-ui/icons/Save";

import ListService from "../Components/ListCard/ListService";
import ImageCardWrapper from "../Components/ImageCard/ImageCardWrapper";
import { Typography } from "@material-ui/core";

const useStyles = makeStyles({
  form: {
    display: "flex",
    flexDirection: "column",
    marginBottom: "10px",
  },
  fieldGroup: {
    maxWidth: "400px",
  },
  field: {
    width: "90%",
    "& input": {
      fontSize: "0.8em",
    },
  },
  largeField: {
    "& input": {
      fontSize: "1.5em",
    },
  },
  center: {
    textAlign: "center",
  },
  gutter: {
    margin: "20px",
  },
});

function ViewList() {
  const classes = useStyles();
  const listService = ListService();
  const NAME = "name";
  const DESC = "description";

  const [list, setList] = useState({});
  const [listTemplate, setListTemplate] = useState({
    name: {
      value: "",
      isEdit: false,
    },
    desc: {
      value: "",
      isEdit: false,
    },
  });

  let { listId } = useParams();

  function handleEdit(event, type) {
    const tmpList = Object.assign({}, listTemplate);
    if (type === NAME) {
      if (tmpList.name.isEdit) {
        const newList = Object.assign({}, list);
        newList.name = listTemplate.name.value;
        listService.updateList(newList);
      }

      tmpList.name.isEdit = !listTemplate.name.isEdit;
      setListTemplate(tmpList);
    } else if (type === DESC) {
      if (tmpList.desc.isEdit) {
        const newList = Object.assign({}, list);
        newList.description = listTemplate.desc.value;
        listService.updateList(newList);
      }

      tmpList.desc.isEdit = !listTemplate.desc.isEdit;
      setListTemplate(tmpList);
    }
  }

  function handleChange(event, type) {
    const tmpList = Object.assign({}, listTemplate);
    if (type === NAME) {
      tmpList.name.value = event.target.value;
    } else if (type === DESC) {
      tmpList.desc.value = event.target.value;
    }
    setListTemplate(tmpList);
  }

  useEffect(() => {
    const storedList = listService.getListById(listId);
    if (storedList !== null) {
      const tmpList = Object.assign({}, listTemplate);
      setList(storedList);

      tmpList.name.value = storedList.name;
      tmpList.desc.value = storedList.description;

      setListTemplate(tmpList);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [listId]);

  return list && list.id ? (
    <div className={classes.gutter}>
      <form className={classes.form}>
        <div className={classes.fieldGroup}>
          <TextField
            className={`${classes.field} ${classes.largeField}`}
            value={listTemplate.name.value}
            disabled={!listTemplate.name.isEdit}
            onChange={(e) => handleChange(e, NAME)}
          />
          <IconButton
            aria-label="edit"
            onClick={(e) => handleEdit(e, NAME)}
            size="small">
            {listTemplate.name.isEdit ? <SaveIcon /> : <EditIcon />}
          </IconButton>
        </div>
        <div className={classes.fieldGroup}>
          <TextField
            className={classes.field}
            value={listTemplate.desc.value}
            disabled={!listTemplate.desc.isEdit}
            onChange={(e) => handleChange(e, DESC)}
          />
          <IconButton
            aria-label="edit"
            onClick={(e) => handleEdit(e, DESC)}
            size="small">
            {listTemplate.desc.isEdit ? <SaveIcon /> : <EditIcon />}
          </IconButton>
        </div>
      </form>

      <ImageCardWrapper imageList={list.images} />
    </div>
  ) : (
    <div className={`${classes.gutter} ${classes.center}`}>
      <Typography variant="subtitle1">There is no list here!</Typography>
    </div>
  );
}

export default ViewList;
