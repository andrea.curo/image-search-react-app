import React, { useEffect, useState } from "react";

import { makeStyles } from "@material-ui/core";

import ImageCard from "./ImageCard";
import ListService from "../ListCard/ListService";

const useStyles = makeStyles({
  wrapper: {
    display: "flex",
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "space-around",
  },
});

function ImageCardWrapper(props) {
  const classes = useStyles();
  const listService = ListService();
  const NO_IMAGES_MESSAGE = "Use the search bar above to find images";

  const [lists, setLists] = useState([]);

  function getAllLists() {
    const storedLists = listService.getAllLists();
    setLists(storedLists);
  }

  // Deps warning is ignored in order to run once on load
  useEffect(() => {
    getAllLists();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className={classes.wrapper}>
      {props.imageList && props.imageList.length > 0
        ? props.imageList.map((image, index) => {
            return (
              <ImageCard
                image={image}
                key={index}
                lists={lists}
                resetLists={getAllLists}
              />
            );
          })
        : NO_IMAGES_MESSAGE}
    </div>
  );
}

export default ImageCardWrapper;
