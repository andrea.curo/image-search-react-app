import React, { useEffect, useState } from "react";

import { makeStyles } from "@material-ui/core";
import Link from "@material-ui/core/Link";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardMedia from "@material-ui/core/CardMedia";
import GetAppIcon from "@material-ui/icons/GetApp";
import AddIcon from "@material-ui/icons/Add";
import IconButton from "@material-ui/core/IconButton";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import Modal from "@material-ui/core/Modal";
import TextField from "@material-ui/core/TextField";
import ImageService from "./imageService";
import ListService from "../ListCard/ListService";

const useStyles = makeStyles((theme) => ({
  cardRoot: {
    height: "250px",
    width: "250px",
    marginBottom: "10px",
  },
  image: {
    height: "250px",
  },
  button: {
    display: "block",
    width: "40%",
    textDecoration: "none",
    color: theme.palette.primary.dark,
    "& span": {
      textOverflow: "ellipsis",
      overflow: "hidden",
    },
  },
  icons: {
    marginLeft: "auto !important",
  },
  actionContent: {
    top: "-64px",
    position: "relative",
    background: "white",
  },
  paper: {
    position: "absolute",
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    transform: "translate(-50%, -50%)",
    top: "50%",
    left: "50%",
  },
  form: {
    display: "flex",
    flexDirection: "column",
  },
  rightButton: {
    marginLeft: "auto",
    marginTop: "10px",
  },
}));

// TODO: split out modal and menu to separate components
function ImageCard(props) {
  const classes = useStyles();
  const imageService = ImageService();
  const listService = ListService();
  
  const [isHover, setIsHover] = useState(false);
  const [menuAnchor, setMenuAnchor] = useState(null);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [newListName, setNewListName] = useState("");
  const [newListDescription, setNewListDescription] = useState("");

  function download(id) {
    imageService.downloadImage(id);
  }

  /*
    Menu Functions
  */
  function handleAddMenu(e) {
    setMenuAnchor(e.currentTarget);
  }

  function handleCloseMenu() {
    setMenuAnchor(null);
  }

  function handleAdd(id) {
    listService.addImageToList(id, props.image);
    handleCloseMenu();
  }

  /*
    Modal Functions
  */
  function handleModalOpen() {
    setMenuAnchor(null);
    setIsModalOpen(true);
    setIsHover(false);
  }

  function handleModalClose() {
    setIsModalOpen(false);
  }

  function handleCreate() {
    listService.createList(props.image, newListName, newListDescription);
    setIsModalOpen(false);
    setNewListName("");
    setNewListDescription("");
    props.resetLists();
  }

  function handleNameChange(e) {
    setNewListName(e.target.value);
  }

  function handleDescChange(e) {
    setNewListDescription(e.target.value);
  }

  // Ensures Menu closes on hover changes
  useEffect(() => {
    if (!isHover) {
      handleCloseMenu();
    }
  }, [isHover]);

  return (
    <Card
      className={classes.cardRoot}
      onMouseEnter={() => setIsHover(true)}
      onMouseLeave={() => setIsHover(false)}>
      <CardMedia
        className={classes.image}
        title={props.image.description}
        image={props.image.urls.thumb}
      />
      {isHover ? (
        <CardActions className={classes.actionContent}>
          <Button
            className={classes.button}
            component={Link}
            href={props.image.user.url}
            target="_blank"
            rel="noopener noreferrer">
            {props.image.user.username}
          </Button>
          <div className={classes.icons}>
            <IconButton
              aria-label="download"
              onClick={() => download(props.image.id)}>
              <GetAppIcon />
            </IconButton>
            <IconButton aria-label="add to list" onClick={handleAddMenu}>
              <AddIcon />
            </IconButton>
          </div>
        </CardActions>
      ) : (
        ""
      )}

      <Modal
        open={isModalOpen}
        onClose={handleModalClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description">
        {
          <div className={classes.paper}>
            <Typography variant="h6" gutterBottom>
              Create a new list
            </Typography>
            <form className={classes.form}>
              <TextField
                fullWidth
                label="Name"
                margin="normal"
                value={newListName}
                onChange={handleNameChange}
              />
              <TextField
                fullWidth
                label="Description"
                margin="normal"
                value={newListDescription}
                onChange={handleDescChange}
              />
              <Button
                variant="contained"
                className={classes.rightButton}
                onClick={handleCreate}>
                Create
              </Button>
            </form>
          </div>
        }
      </Modal>

      <Menu
        id="simple-menu"
        anchorEl={menuAnchor}
        keepMounted
        open={Boolean(menuAnchor)}
        onClose={handleCloseMenu}>
        <MenuItem>My Lists</MenuItem>
        <hr />
        {props.lists.length > 0 ? (
          props.lists.map((list, index) => {
            return (
              <MenuItem
                onClick={() => {
                  handleAdd(list.id);
                }}
                key={index}>
                {list.name}
              </MenuItem>
            );
          })
        ) : (
          <MenuItem>No lists yet!</MenuItem>
        )}
        <hr />
        <MenuItem onClick={handleModalOpen}>Create List</MenuItem>
      </Menu>
    </Card>
  );
}

export default ImageCard;
