const ImageService = () => {
  const BASE_PATH = process.env.REACT_APP_BASE_PATH || "";
  const PROTOCOL = process.env.REACT_APP_PROTOCOL || "";

  const queryImages = async (searchText) => {
    const request = {
      method: "GET",
    };
    const encodedSearchText = encodeURIComponent(searchText);

    return fetch(
      `${PROTOCOL}://${BASE_PATH}/api/images?searchText=${encodedSearchText}`,
      request
    )
      .then((res) => res.json())
      .catch((err) => {
        console.log(`Error on api call ${err}`);
        return {
          error: err,
        };
      });
  };

  const downloadImage = async (id) => {
    return window.open(`${PROTOCOL}://${BASE_PATH}/api/images/${id}/download`);
  };

  return {
    queryImages,
    downloadImage,
  };
};

export default ImageService;
