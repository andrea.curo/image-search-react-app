import React from "react";
import ListCard from "./ListCard";

import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles({
  listWrapper: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-around",
    flexWrap: "wrap",
  },
});

function ListCardWrapper(props) {
  const classes = useStyles();

  return (
    <div className={classes.listWrapper}>
      {props.lists && props.lists.length > 0
        ? props.lists.map((list, index) => {
            return <ListCard list={list} key={index} />;
          })
        : "No lists yet!"}
    </div>
  );
}

export default ListCardWrapper;
