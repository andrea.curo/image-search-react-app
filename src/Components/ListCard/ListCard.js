import React from "react";

import { Link } from "react-router-dom";

import { makeStyles } from "@material-ui/core";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardMedia from "@material-ui/core/CardMedia";
import CardHeader from "@material-ui/core/CardHeader";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles({
  cardRoot: {
    height: "250px",
    width: "250px",
    marginBottom: "10px",
  },
  image: {
    height: "250px",
  },
  link: {
    textDecoration: "none",
  },
});

function ListCard(props) {
  const classes = useStyles();

  return (
    <div>
      <Link to={`/lists/${props.list.id}`} className={classes.link}>
        <Card className={classes.cardRoot}>
          <CardHeader title={props.list.name} />
          <CardMedia
            className={classes.image}
            image={props.list.images[0].urls.thumb}
            title={props.list.description}
          />
          <CardActions>
            <Button size="small" color="primary">
              View List
            </Button>
          </CardActions>
        </Card>
      </Link>
    </div>
  );
}

export default ListCard;
