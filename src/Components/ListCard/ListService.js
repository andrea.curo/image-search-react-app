import uuid from "react-uuid";

// TODO: Add error handling
const ListService = () => {
  const COLLECTIONS = "collections";

  const getAllLists = () => {
    const result = localStorage.getItem(COLLECTIONS);
    if (result) {
      const lists = JSON.parse(result);
      return lists && lists.length > 0 ? lists : [];
    } else {
      return [];
    }
  };

  const saveLists = (newLists) => {
    if (newLists && newLists.length > 0) {
      const strLists = JSON.stringify(newLists);
      localStorage.setItem(COLLECTIONS, strLists);
    }
  };

  const createList = (image, name, description) => {
    const newUuid = uuid();
    const newList = {
      id: newUuid,
      images: [image],
      name: name,
      description: description,
    };
    const allLists = getAllLists();
    allLists.push(newList);
    localStorage.setItem(COLLECTIONS, JSON.stringify(allLists));
  };

  const getListById = (id) => {
    const lists = getAllLists();
    const selectedList = lists.filter((list) => list.id === id);
    if (selectedList.length > 0) {
      return selectedList[0];
    }
    return null;
  };

  const addImageToList = (listId, image) => {
    const lists = getAllLists();

    // Some() allows for short-circuiting
    lists.some((list) => {
      if (list.id === listId) {
        list.images.push(image);
        return true;
      }
      return false;
    });

    saveLists(lists);
  };

  const updateList = (newList) => {
    const lists = getAllLists();

    lists.some((list) => {
      if (list.id === newList.id) {
        list.name = newList.name;
        list.description = newList.description;
        return true;
      }
      return false;
    });

    saveLists(lists);
  };

  return {
    getAllLists,
    createList,
    getListById,
    addImageToList,
    updateList,
  };
};

export default ListService;
