import React from "react";
import { render } from "@testing-library/react";
import App from "./App";

test("Loads Application", () => {
  const { getByText } = render(<App />);
  const linkElement = getByText(/image search/i);
  expect(linkElement).toBeInTheDocument();
});
